A simple script in fish for handling different screenshot functions. It can save, copy and upload screenshots.
I made this script to add screenshot keybinds in dwm easier.
Change the variables in the code acording to your tastes.  
***Usage***  
``./screenshot save`` for saving screenshot  
``./screenshot copy`` for copying screenshot  
``./screenshot upload`` for uploading file  
See in-code comments for more details
